import guitar.Guitar;
import guitar.Electronic;
import guitar.Acoustic;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Guitar> guitarFactory =new ArrayList<Guitar>();

        Guitar tyrantBloodstorm = new Electronic("Dean Guitars", "Tyrant Bloodstorm", 2008, "Humbucker");
        Guitar gibsonExplorer = new Electronic("Gibson", "Explorer", 2008, "Humbucker");
        Guitar martinLittleMartin = new Acoustic("Martin", "LX1E Little Martin", 2005);

        guitarFactory.add(tyrantBloodstorm);
        guitarFactory.add(gibsonExplorer);
        guitarFactory.add(martinLittleMartin);

        for (Guitar guitar:guitarFactory) {
            guitar.Playable();
        }
    }
}
