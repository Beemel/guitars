package guitar;

public class Acoustic extends Guitar implements Playable{

    public Acoustic(String brand, String model, int yearManufactured) {
        super(brand, model, yearManufactured);
    }

    @Override
    public void makeSound() {
        System.out.println("Best enjoyed clean with a side of crisp singer songwriter voice.");
    }

    @Override
    public void Playable() {

    }
}
