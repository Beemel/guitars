package guitar;

public abstract class Guitar  implements Playable {
    private String brand;
    private static String model;
    private int yearManufactured;


    public Guitar(String brand, String model, int yearManufactured) {
        this.brand = brand;
        this.model = model;
        this.yearManufactured = yearManufactured;
    }

    public abstract void makeSound();

    public abstract void Playable();{
        makeSound();

    }

}
