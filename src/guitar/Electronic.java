package guitar;

public class Electronic extends Guitar implements Playable {

    private String pickUps;

    public Electronic(String brand, String model, int yearManufactured, String pickUps) {
        super(brand, model, yearManufactured);
        this.pickUps = pickUps;
    }

    @Override
    public void makeSound() {
        System.out.println("Distorted in combination with some fast double bass and in combination with deep growling " +
                "it is a like music for the ears. Literally!");

    }

    @Override
    public void Playable() {

    }

}
